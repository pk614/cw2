"""Final project, part 2"""
import numpy as np
import matplotlib.pyplot as plt
from p7 import bmodel
#from m1 import bmodel as bm #assumes p2.f90 has been compiled with: f2py -c p2.f90 -m m1


def simulate_jacobi(n,input_num=(10000,1e-8),input_mod=(1,1,1,2,1.5),display=False):
    """ Solve contamination model equations with
        jacobi iteration.
        Input:
            input_num: 2-element tuple containing kmax (max number of iterations
                        and tol (convergence test parameter)
            input_mod: 5-element tuple containing g,k_bc,s0,r0,t0 --
                        g: bacteria death rate
                        k_bc: r=1 boundary condition parameter
                        s0,r0,t0: source function parameters
            display: if True, a contour plot showing the final concetration field is generated
        Output:
            C,deltac: Final concentration field and |max change in C| each iteration
    """
    #Set model parameters------

    kmax,tol = input_num
    g,k_bc,s0,r0,t0 = input_mod
    #-------------------------------------------
    #Set Numerical parameters
    Del = np.pi/(n+1)
    r = np.linspace(1,1+np.pi,n+2)
    t = np.linspace(0,np.pi,n+2) #theta
    tg,rg = np.meshgrid(t,r) # r-theta grid

    #Factors used in update equation
    rinv2 = 1.0/(rg*rg)
    fac = 1.0/(2 + 2*rinv2+Del*Del*g)
    facp = (1+0.5*Del/rg)*fac
    facm = (1-0.5*Del/rg)*fac
    fac2 = fac*rinv2

    #set initial condition/boundary conditions
    C = (np.sin(k_bc*tg)**2)*(np.pi+1.-rg)/np.pi

    #set source function, Sdel2 = S*del^2*fac
    Sdel2 = s0*np.exp(-20.*((rg-r0)**2+(tg-t0)**2))*(Del**2)*fac

    deltac = []
    Cnew = C.copy()

    #Jacobi iteration
    for k in range(kmax):
        #Compute Cnew
        Cnew[1:-1,1:-1] = Sdel2[1:-1,1:-1] + C[2:,1:-1]*facp[1:-1,1:-1] + C[:-2,1:-1]*facm[1:-1,1:-1] + (C[1:-1,:-2] + C[1:-1,2:])*fac2[1:-1,1:-1] #Jacobi update

        #Compute delta_p
        deltac += [np.max(np.abs(C-Cnew))]
        C[1:-1,1:-1] = Cnew[1:-1,1:-1]
        if k%1000==0: print("k,dcmax:",k,deltac[k])
        #check for convergence
        if deltac[k]<tol:
            print("Converged,k=%d,dc_max=%28.16f " %(k,deltac[k]))
            break

    deltac = deltac[:k+1]

    if display:
        plt.figure()
        plt.contour(t,r,C,50)
        plt.xlabel('theta')
        plt.ylabel('r')
        plt.title('Final concentration field')
        plt.show()

    return C,deltac



def simulate_over(n,input_num=(10000,1e-8),input_mod=(1,1,1,2,1.5),display=False):
    """ Solve contamination model equations with
        OSI method, input/output same as in simulate_jacobi above
    """
    #Set model parameters------

    kmax,tol = input_num
    g,k_bc,s0,r0,t0 = input_mod
    Del = np.pi/(n+1)
    r = np.linspace(1,1+np.pi,n+2)
    t = np.linspace(0,np.pi,n+2) #theta
    tg,rg = np.meshgrid(t,r) # r-theta grid

    #Factors used in update equation
    rinv2 = 1.0/(rg*rg)
    fac = 1.0/(2 + 2*rinv2+Del*Del*g)
    facp = (1+0.5*Del/rg)*fac
    facm = (1-0.5*Del/rg)*fac
    fac2 = fac*rinv2

    #set initial condition/boundary conditions
    C = (np.sin(k_bc*tg)**2)*(np.pi+1.-rg)/np.pi

    #set source function, Sdel2 = S*del^2*fac
    Sdel2 = s0*np.exp(-20.*((rg-r0)**2+(tg-t0)**2))*(Del**2)*fac

    deltac = []
    Cnew = C.copy()


    #Add code here
    #Over-step iteration
    iteration=[]
    for k in range(kmax):
         for i in range(1,n+1):
        #Compute Cnew
             Cnew[i,1:-1] = 0.5* (3*(Sdel2[i,1:-1] + C[i+1,1:-1]*facp[i,1:-1] +(Cnew[i,:-2] + C[i,2:])*fac2[i,1:-1] + Cnew[i-1,1:-1]*facm[i,1:-1]) - (C[i,1:-1]))  #Over step update
        #Compute delta_p
         deltac += [np.max(np.abs(C-Cnew))]
         C[1:-1,1:-1] = Cnew[1:-1,1:-1]
         if k%1000==0: print("k,dcmax:",k,deltac[k])
         iteration.append(k)
        #check for convergence
         if deltac[k]<tol:
            print("Converged,k=%d,dc_max=%28.16f " %(k,deltac[k]))
            break

    deltac = deltac[:k+1]

    if display:
        plt.figure()
        plt.contour(t,r,C,50)
        plt.xlabel('theta')
        plt.ylabel('r')
        plt.title('Final concentration field')
        plt.show()


    return C,deltac,iteration


def performance():
    """This function analyzes the performance of python and fortran routine for solving contamination problem
    using over-step iteration. From figure 1, it is evident that fortran is much quicker in terms of time
    when compared to python. This is primarily due to an extra loop in python routine which slows the process
    whereas in fortran the speed is independent of number of loops used. The lower speed for python routine can also be
    attributed to larger number of iterations required to converge when compared to fortran and this can be
    seen in figure 2."""
    N = []
    timer1=[]
    timer2=[]
    nvalues=[10,20,30,40,50]
    K1=np.zeros(len(nvalues))
    K2=np.zeros(len(nvalues))
    for l,n in enumerate(nvalues):
        import time
        t0=time.time()
        _,_,iteration = simulate_over(n,input_num=(10000,1e-8),input_mod=(1,1,1,2,1.5),display=False)
        t1=time.time()
        print(l,iteration[-1])
        K1[l]=iteration[-1]
        total1=t1-t0
        print("n=%d, time taken = %18.16f " %(n,total1))
        timer1.append(total1)
        N.append(n)

    for q,n in enumerate(nvalues):
        import time
        t0=time.time()
        iteration = bmodel.simulate_over(n)
        t1=time.time()
        K2[q]= iteration
        total2=t1-t0
        print("n=%d, time taken = %18.16f, iteration=%d " %(n,total2,K2[q]))
        timer2.append(total2)

    fig=plt.figure(1)
    plt.plot(N,timer1)
    plt.plot(N,timer2)
    plt.xlabel('N')
    plt.ylabel('Time taken (seconds)')
    plt.legend(('Python','Fortran'))
    plt.show()
    fig=plt.figure(2)
    plt.plot(N,K1)
    plt.plot(N,K2)
    plt.xlabel('N')
    plt.ylabel('Iterations to converge')
    plt.legend(('Python','Fortran'))
    plt.show()



    return None


def analyze(n,tgs,input_num=(10000,1e-8),input_mod=(1,2,2,1+(np.pi/2),1.5),display=False):
    """Analyze influence of modified
    boundary condition on contamination dynamics
        Add input/output variables as needed.
    """
    kmax,tol = input_num
    g,k_bc,s0,r0,t0 = input_mod
    Del = np.pi/(n+1)
    r = np.linspace(1,1+np.pi,n+2)
    t = np.linspace(0,np.pi,n+2) #theta
    tg,rg = np.meshgrid(t,r) # r-theta grid

    #Factors used in update equation
    rinv2 = 1.0/(rg*rg)
    fac = 1.0/(2 + 2*rinv2+Del*Del*g)
    facp = (1+0.5*Del/rg)*fac
    facm = (1-0.5*Del/rg)*fac
    fac2 = fac*rinv2

    #set initial condition/boundary conditions
    C1 = np.exp(-10*(tg-tgs)**2)*(np.sin(k_bc*tg)**2)*(np.pi+1.-rg)/np.pi

    #set source function, Sdel2 = S*del^2*fac
    Sdel2 = s0*np.exp(-20.*((rg-r0)**2+(tg-t0)**2))*(Del**2)*fac

    deltac = []
    Cnew1 = C1.copy()


    #Add code here
    #Over-step iteration
    iteration=[]
    for k in range(kmax):
         for i in range(1,n+1):
        #Compute Cnew
             Cnew1[i,1:-1] = 0.5* (3*(Sdel2[i,1:-1] + C1[i+1,1:-1]*facp[i,1:-1] +(Cnew1[i,:-2] + C1[i,2:])*fac2[i,1:-1] + Cnew1[i-1,1:-1]*facm[i,1:-1]) - (C1[i,1:-1]))  #Over step update
        #Compute delta_p
         deltac += [np.max(np.abs(C1-Cnew1))]
         C1[1:-1,1:-1] = Cnew1[1:-1,1:-1]
         if k%1000==0: print("k,dcmax:",k,deltac[k])
         iteration.append(k)
        #check for convergence
         if deltac[k]<tol:
            print("Converged,k=%d,dc_max=%28.16f " %(k,deltac[k]))
            break

    deltac = deltac[:k+1]
    C,_,_ = simulate_over(n,input_num=(10000,1e-8),input_mod=(1,2,2,1+(np.pi/2),1.5),display=False)
    if display:
        plt.figure(1)
        plt.contour(t,r,C,50)
        #plt.colorbar()
        plt.xlabel('theta')
        plt.ylabel('r')
        plt.title('Final concentration field without antibacterial agents')
        plt.show()
        plt.figure(2)
        plt.contour(t,r,C1,50)
        #plt.colorbar()
        plt.xlabel('theta')
        plt.ylabel('r')
        plt.title('Final concentration field with antibacterial agents')
        plt.show()


    return None


if __name__=='__main__':
    #Add code below to call performance and analyze
    #and generate figures you are submitting in
    #your repo.
    input=()
