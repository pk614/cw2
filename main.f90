program part2
	use bmodel
    integer :: quad_type,n
    real(kind=8),dimension(0:n+1,0:n+1)::C
    real(kind=8):: cpu_t1,cpu_t2, clock_time
    integer(kind=8):: clock_t1, clock_t2, clock_rate
		open(unit=10, file='data.in')
	  	read(10,*) n
			read(10,*) quad_type
		close(10)

!read data from data.in


    !compute concentration
    call system_clock(clock_t1)
    call cpu_time(cpu_t1)

		if (quad_type==1) then
				call simulate_jacobi(n,C)
		else
				call simulate_over(n,C)
		end if
    call cpu_time(cpu_t2)
    print *,'elasped cpu time (seconds)=',cpu_t2-cpu_t1
    call system_clock(clock_t2,clock_rate)
    print *,'elasped wall time (seconds)=',dble(clock_t2-clock_t1)/dble(clock_rate)


end program part2
